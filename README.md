

# react-chatgpt

react-chatgpt is a React project that uses modules/server.js, modules/client.js, and webpack.config.js to create a chatbot application.

## Installation

To install react-chatgpt, you will need the following dependencies: 
* babel-cli 
* babel-core 
* babel-eslint 
* babel-loader 
* babel-preset-es2015 
* babel-preset-react 
* babel-preset-react-hmre 
* babel-preset-stage-1 
* body-parser 
* bundle-loader 
* compression 
* css-loader  
* dotenv  
* eslint  
* eslint-config-rackt  
* eslint-plugin-react  
* expect  
* express  
* extract text webpack plugin  												    
* file loader  			     
* helmet    
* hpp    
* if env    * karma    * karma chrome launcher    * karma mocha    * karma mocha reporter    * karma sourcemap loader    * karma webpack     * mocha     * morgan     * node fetch     * null loader     * postcss loader      * react      * react dom      * react project      * react router      * react title component      * source map support      * style loader      * url loader      * webpack       webpack dev server

Once you have installed all of the dependencies, you can run `npm start` to start the application in development mode or `npm run react project:start:prod` to start it in production mode. You can also run `npm test` to run the Karma tests for the application.