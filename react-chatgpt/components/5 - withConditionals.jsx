// create a react component based on react-bootstrap navbar named NavBarTestChatGpt using react-router-dom version 6 
// with links to Home, KindaLab, About Us, Example Link and configurate every classname Link depends if is active

import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';

const NavBarTestChatGpt = () => {
    const location = useLocation();

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">TestChatGpt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Link to="/" className={location.pathname === '/' ? 'nav-link active' : 'nav-link'}>Home</Link>
                    <Link to="/kindalab" className={location.pathname === '/kindalab' ? 'nav-link active' : 'nav-link'}>KindaLab</Link>
                    <Link to="/aboutus" className={location.pathname === '/aboutus' ? 'nav-link active' : 'nav-link'}>About Us</Link>
                    <Link to="/examplelink" className={location.pathname === '/examplelink' ? 'nav-link active' : 'nav-link'}>Example Link</Link>

                </Nav>

            </Navbar.Collapse>

        </Navbar>

    );
};

export default NavBarTestChatGpt;