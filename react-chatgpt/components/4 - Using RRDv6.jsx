// create a react component based on react-bootstrap navbar named NavBarTestChatGpt using react-router-dom version 6 with links to Home, KindaLab, About Us, Example Link

import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NavBarTestChatGpt = () => {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">TestChatGpt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Link to="/Home" className="nav-link">Home</Link>
                    <Link to="/KindaLab" className="nav-link">KindaLab</Link>
                    <Link to="/AboutUs" className="nav-link">About Us</Link>
                    <Link to="/ExampleLink" className="nav-link">Example Link</Link>

                </Nav>

            </Navbar.Collapse>

        </Navbar>

    );

};

export default NavBarTestChatGpt;