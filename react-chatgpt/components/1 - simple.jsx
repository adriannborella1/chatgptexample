// create a react component based on bootstrap navbar

import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

const MyNavbar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">My App</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavItem>Home</NavItem>
          <NavItem>About</NavItem>
          <NavItem>Contact</NavItem>
        </Nav>
      </Navbar.Collapse> 
    </Navbar> 

  ) 
} 

 export default MyNavbar;
