
// create a react component based on react-bootstrap navbar named NavBarTestChatGpt

import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';

const NavBarTestChatGpt = () => {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">TestChatGpt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#about">About</Nav.Link>
                    <Nav.Link href="#contact">Contact Us</Nav.Link>
                </Nav>

                <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />

                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

                </form>

            </Navbar.Collapse>

        </Navbar>);
};
export default NavBarTestChatGpt;