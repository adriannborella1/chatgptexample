"""
Escribir una clase en python llamada círculo que contenga un radio, 
con un método que devuelva el área y 
otro que devuelva el perímetro del círculo.

Si se instancia la clase con radio <= 0 mostrar una excepción indicando un error amigable al
usuario e impidiendo la instanciación.

Si printeamos el objeto creado debe mostrarse una representación amigable.

El objeto debe tener su atributo radio modificable, si se le intenta setear un valor <= 0
mostrar un error y no permitir modificación.

Permitir la multiplicación del circulo: Circulo * n debe devolver un nuevo objeto con el radio
multiplicado por n. No permitir la multiplicación por números <= 0
"""



class Circle:
    def __init__(self, radius):
        if radius <= 0:
            raise ValueError("Radius must be greater than 0")
        self.radius = radius

    def __repr__(self):
        return f"Circle({self.radius})"

    @property
    def area(self):
        return 3.14 * self.radius ** 2

    @property
    def perimeter(self):
        return 2 * 3.14 * self.radius

    @radius.setter  # setter para el atributo radius del objeto círculo 
    def radius(self, value):  # si el valor es <= 0 mostrar un error y no permitir modificación 
        if value <= 0:  # si se intenta setear un valor <= 0 mostrar un error y no permitir modificación 
            raise ValueError("Radius must be greater than 0")  
        self._radius = value

    # Permitir la multiplicación del circulo: Circulo * n debe devolver un nuevo objeto con el radio multiplicado por n. No permitir la multiplicación por números <= 0 
    def __mul__(self, other):   # sobrecargar el operador de multiplicación para que devuelva un objeto círculo con el radio multiplicado por other (otro objeto) 
        if other <= 0:   # si other es menor o igual a cero mostrar un error y no permitir la multiplicación 
            raise ValueError("Cannot multiply by a number less or equal to zero")  

        return Circle(self.radius * other)   # devolver un nuevo objeto círculo con el radio multiplicado por other (otro objeto) 

    

# Test code for the Circle class 
import pytest   # importar pytest para realizar los tests unitarios de la clase Círculo 
def test_circle_init():   # test para comprobar que al instanciar la clase Círculo con