"""
Crear una matriz de 5x5 randomizada con números enteros, encontrar secuencia de 4
números consecutivos horizontal o vertical y si se encuentra mostrar la posición inicial y
final.
"""
import random

def generate_matrix():
    matrix = [[random.randint(1, 9) for _ in range(5)] for _ in range(5)]
    return matrix

def find_sequence(matrix): 
    for row in range(len(matrix)): 
        for col in range(len(matrix[row])): 

            # check horizontal sequence 
            if col <= len(matrix[row]) - 4: 

                # check if the sequence is found 
                if matrix[row][col] == matrix[row][col + 1] - 1 == matrix[row][col + 2] - 2 == matrix[row][col + 3] - 3: 

                    # print the position of the sequence found 
                    print("Sequence found at ({}, {}) to ({}, {})".format(row, col, row, col + 3))

            # check vertical sequence 
            if row <= len(matrix) - 4: 

                # check if the sequence is found 
                if matrix[row][col] == matrix[row + 1][col] - 1 == matrix[row + 2][col] - 2 == matrix[row + 3][col] - 3: 

                    # print the position of the sequence found 
                    print("Sequence found at ({}, {}) to ({}, {})".format(row, col, row + 3, col))

                    
# Test Unitarios para cada función usando Pytest:                    
import pytest                                                          # Importamos la librería Pytest.  
import random                                                          # Importamos la librería Random.  

def test_generate_matrix():                                         # Creamos una función de test para la función generate_matrix().  
    result = generate_matrix()                                     # Guardamos el resultado de la función en una variable.  
    assert type (result) is list and len (result) == 5              # Comprobamos que el resultado es una lista de 5 elementos.  

def test_find_sequence():                                         # Creamos una función de test para la
    pass
# Aca se corto por el limite configurado en el plugin