"""
SIMPLE
Hacer una función que genere una lista de diccionarios que contengan id y edad, donde
edad sea un número aleatorio entre 1 y 100 y la longitud de la lista sea de 10
elementos. retornar la lista.
Hacer otra función que reciba lo generado en la primer función y ordenarlo de mayor a
menor. Printear el id de la persona más joven y más vieja. Devolver la lista ordenada
"""
# generate a python program with each function tested using doctest



import random 

def generate_list():
    """Generates a list of 10 dictionaries with an id and age, where age is a random number between 1 and 100. 
    Returns the list. 
    """

    list_dict = [] #initialize empty list

    for i in range(10): #generate 10 dictionaries
        dict_temp = {'id': i, 'age': random.randint(1,100)} #create dictionary with id and age
        list_dict.append(dict_temp) #add dictionary to the list

    return list_dict #return the list of dictionaries


def sort_list(list_dict): 
    """Sorts the inputted list of dictionaries from highest to lowest age. Prints the id of the youngest and oldest person. Returns the sorted list. 
    """

    sorted_list = sorted(list_dict, key=lambda x: x['age'], reverse=True) #sort from highest to lowest age

    print("The id of the youngest person is:", sorted_list[-1]['id']) #print id of youngest person (last element in sorted list) 
    print("The id of the oldest person is:", sorted_list[0]['id']) #print id of oldest person (first element in sorted list) 

    return sorted_list #returns the sorted list




import random

def generate_list():
    """Generates a list of dictionaries with id and age.

    >>> generate_list()
    [{'id': 0, 'age': 28}, {'id': 1, 'age': 45}, {'id': 2, 'age': 97}, {'id': 3, 'age': 4}, {'id': 4, 'age': 60}, {'id': 5, 'age': 11}, {'id': 6, 'age': 79}, {'id': 7, 'age': 15}, {'id': 8, 'age': 68}, {'id': 9, 'age': 32}]
    """

    # Generate a list of 10 dictionaries with random ages between 1 and 100.
    lst = []
    for i in range(10):
        lst.append({'id': i, 
                    'age': random.randint(1, 100)})

    return lst

# queda a la mitad por la cantidad de caracteres al parecer