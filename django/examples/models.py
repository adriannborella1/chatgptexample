from django.db import models

# Create your models here.

# Create A models named UsersChatGPT

class UsersChatGPT(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat_id = models.IntegerField()
    gpt_id = models.IntegerField()

# Create A models named UsersChatGPT with fields first_name, last_name, email, age, create_date and isActive

class UsersChatGPT(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    age = models.IntegerField() 
    create_date = models.DateTimeField(auto_now_add=True) 
    isActive = models.BooleanField(default=True)

    def __str__(self): 
        return self.first_name

# Create A models named UsersChatGPT with fields first_name with max_length = 150, last_name with max_length 
# equals to 80, email, age, create_date and isActive 
# and in the Meta Class the properties abstract=True, table_name = "UserTest" and PropertyTest equalts to 22

class UsersChatGPT(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=80)
    email = models.EmailField()
    age = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)
    isActive = models.BooleanField()

    class Meta:
        abstract = True
        table_name = "UserTest"
        PropertyTest = 22